import { Component, OnInit ,ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {


  @ViewChild('name') name! : ElementRef;

  
  constructor() { }

  ngOnInit(): void {
  } 

stratQuiz(){
   localStorage.setItem("name",this.name.nativeElement.value);
  // console.log(this.input.nativeElement.value);
}



}
