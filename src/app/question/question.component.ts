import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { QuestionService } from '../service/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  
  public name : string="";
  public questionList  : any = [];
  public CurrentQuestions:number = 0;
  public points :number=0;
  counter=60;
  correctAnswer: number =0 ;
  inCorrectAnswer:number =0 ; 
  interval$ : any;
  progress:string="0";
  isQuizCompleted : boolean = false;
  
  constructor( private questionService : QuestionService) { }

  ngOnInit(): void {
    this.name = localStorage.getItem("name")!;
    this.getAllQuestions();
    this.stratCounter();
  }

getAllQuestions(){
 this.questionService.getQuestionJson()
 .subscribe(res =>{
  // console.log(res.questions)
   this.questionList = res.questions ;
  
 })
}

nextQuestions(){
  this.CurrentQuestions++;
}

previousQuestion(){
   this.CurrentQuestions--;
}

answer(CurrentQno : number,option : any){

  if(CurrentQno === this.questionList.length){
    this.isQuizCompleted = true;
    this.stopCounter();
  }
  if(option.correct){
    // this.points +=10 ;
    this.points = this.points + 10;
    this.correctAnswer++;
    setTimeout(() =>{
      this.CurrentQuestions++;
      this.resetCounter();
      this.getProgressPercent();
    },1000);
    
  }else{
      //  this.points-=10;
        
       setTimeout(() =>{
        this.CurrentQuestions++;
        this.inCorrectAnswer--;
        this.resetCounter();
        this.getProgressPercent();
       },1000);

       this.points = this.points - 10; 
  }
}

stratCounter(){
   this.interval$ = interval(1000)
   .subscribe(val=>{
    this.counter--;
    if(this.counter===0){
       this.CurrentQuestions++;
       this.counter=60;
       this.points = this.points - 10;
    }
   });
   setTimeout(() => {
       this.interval$.unsubscribe();
   }, 60000);
}

stopCounter(){
  this.interval$.unsubscribe();
  this.counter=0;
}

resetCounter(){
  this.stopCounter();
  this.counter=60;
  this.stratCounter();
}

resetQuiz(){
  this.resetCounter();
  this.getAllQuestions();
  this.points=0;
  this.counter=60;
  this.CurrentQuestions=0;
  this.progress="0"
}


getProgressPercent(){
  this.progress = ((this.CurrentQuestions/this.questionList.length )* 100).toString();
  return this.progress;
}

}
